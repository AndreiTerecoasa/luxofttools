import sublime
import sys
import os
import sublime_plugin
from threading import Thread
 
sys.path.append(os.path.join(os.path.dirname(__file__), "requests"))
import requests


class luxoftgetkeyCommand(sublime_plugin.TextCommand):
	url = ""
	responses = []
	threads = []
	region = ""
	view = ""
	edit = ""
	port = ""
	settings = None
	toTable = None
	def run(self, edit):
		self.settings = sublime.load_settings("Preferences.sublime-settings")
		self.port = self.settings.get("luxoft_tools_port", "8001")
		self.url = "http://10.2.38.31:" + self.port + "/api/tokey"
		if self.settings.get('luxoft_tools_my_section', 'default') == 'default':
			sublime.run_command('registernewtable')
			return
		self.toTable = self.settings.get('luxoft_tools_my_section', 'default')
		try:
			self.view = self.view
			self.edit = edit
			sel = self.view.sel()
			if len(sel):
				for (i, s) in enumerate(sel):
					if s == "":
						continue
					else:
						text = self.view.substr(s)
						self.getKeyForSelection(text,i)
		except requests.exceptions.ReadTimeout:
			sublime.error_message("Apelul de API a durat cam mult. Try again ^^");
	def on_done(self,index):
		print(self.responses[index])
		if index == -1:
			return
		replace = "l('" + self.responses[index] + "')";
		self.view.replace(self.edit,self.view.sel()[0], replace)

	def getKeyForSelection(self, text, i):
		toReturnB = ''
		toReturnE = ''
		letsPreserveThis = ''
		toApi = ''
		print(text)
		if (text[0] == "\"" and text[-1] == "\"") or (text[0] == "'" and text[-1] == "'"):
			letsPreserveThis = text[0]
			text = text[1:-1]
			if text[0] == ' ':
				toReturnB = letsPreserveThis + ' ' + letsPreserveThis + ' + '
				text = text[1:]
			if text[-1] == ' ':
				toReturnE = ' + ' +  letsPreserveThis + ' ' + letsPreserveThis
				text = text[:-1]
		toReturn = self.callApi(text)
		if toReturn != "":
			replace = toReturnB + toReturn + toReturnE
			self.view.replace(self.edit, self.view.sel()[i], replace)
		return

	def callApi(self, text):
		print(self.toTable)
		try:
			data = {'data':text, 'table': self.toTable}
			response = requests.post(self.url,data=data, timeout=0.500);
			response = response.json();
			if (response['type'] == -1):
				current_table = self.settings.get("luxoft_tools_my_section", 'default')
				sublime.message_dialog(response['result'] + ": Tabelul setat nu exista [" + current_table + "]");
			else:
				replace = "l('" + response['result'] + "')";
				return replace
			return "";
		except requests.exceptions.ReadTimeout:
			sublime.error_message("Apelul de API a durat cam mult. Try again ^^");