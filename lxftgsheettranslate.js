var translateKey = 'trnsl.1.1.20170113T131546Z.aac676edd83f4074.e41a0b73bcee43f4e1330b042172ecb322a4a9bc';

function TRANSLATEFR(arg) {
  if(arg == "") return;
  var response = UrlFetchApp.fetch('https://translate.yandex.net/api/v1.5/tr.json/translate?key='+translateKey+'&text=' + encodeURIComponent(arg) + '&lang=fr&format=plain');
  var response = JSON.parse(response.getContentText());
  return response.text;
}

function TRANSLATEHI(arg) {
   if(arg == "") return;
  var response = UrlFetchApp.fetch('https://translate.yandex.net/api/v1.5/tr.json/translate?key='+translateKey+'&text=' + encodeURIComponent(arg) + '&lang=hi&format=plain');
  var response = JSON.parse(response.getContentText());
  return response.text;
}
function TRANSLATECHI(arg) {
   if(arg == "") return;
  var response = UrlFetchApp.fetch('https://translate.yandex.net/api/v1.5/tr.json/translate?key='+translateKey+'&text=' + encodeURIComponent(arg) + '&lang=zh&format=plain');
  var response = JSON.parse(response.getContentText());
  return response.text;
}