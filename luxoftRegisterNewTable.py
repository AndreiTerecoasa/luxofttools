import sublime
import sys
import os
import sublime_plugin
from threading import Thread
 
sys.path.append(os.path.join(os.path.dirname(__file__), "requests"))
import requests

class registernewtableCommand(sublime_plugin.TextCommand):
	settings = None
	url = None
	port = None
	def run(self, edit):
		self.settings = sublime.load_settings("Preferences.sublime-settings")
		self.port = self.settings.get("luxoft_tools_port", "8001")
		self.url = "http://10.2.38.31:" + self.port + "/api/registerNewTable"

		current_table = self.settings.get("luxoft_tools_my_section", 'default')
		sublime.message_dialog("Tabelul pe care lucrezi acum este: " + current_table);
		if sublime.ok_cancel_dialog("Vrei sa schimbi tabelul?", "Da"):
			sublime.Window.show_input_panel(self.view.window(), "caption", "", self.tableInputDone, None, None)
		print(current_table)
	def tableInputDone(self, text):
		self.settings.set('luxoft_tools_my_section', text);
		sublime.save_settings("Preferences.sublime-settings");
		sublime.Window.status_message(self.view.window(),'Numele tabelului a fost setat')
		self.callApi(text)
	def on_cancel():
		print("oups")

	def callApi(self, text):
		try:
			data = {'table': text}
			response = requests.post(self.url,data=data, timeout=0.500);
			response = response.json()['result'];
			print(response);
		except requests.exceptions.ReadTimeout:
			sublime.error_message("Apelul de API a durat cam mult. Try again ^^");

class viewtablesCommand(sublime_plugin.TextCommand):
	tables = [];
	def callApi(self):
		try:
			self.settings = sublime.load_settings("Preferences.sublime-settings")
			self.port = self.settings.get("luxoft_tools_port", "8001")
			response = requests.post("http://10.2.38.31:" + self.port + "/api/getAllTables");
			print(response);
			response = response.json();
			self.tables = response;
		except requests.exceptions.ReadTimeout:
			sublime.error_message("Apelul de API a durat cam mult. Try again ^^");
	def run(self, edit):
		self.callApi();
		sublime.active_window().show_quick_panel(self.tables, self.selectedItem, 0, 0, 0)

	def selectedItem(self, index):
		self.settings.set('luxoft_tools_my_section', self.tables[index]);
		sublime.save_settings("Preferences.sublime-settings");
		current_table = self.settings.get("luxoft_tools_my_section", 'default')
		sublime.message_dialog("Tabelul pe care lucrezi acum este " + current_table);